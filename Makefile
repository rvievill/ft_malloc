# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: rvievill <rvievill@student.42.fr>          +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2017/11/15 10:57:06 by rvievill          #+#    #+#              #
#    Updated: 2017/12/08 14:15:03 by rvievill         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

ifeq ($(HOSTTYPE),)
	HOSTTYPE := $(shell uname -m)_$(shell uname -s)
endif

CC= clang
NAME = libft_malloc_$(HOSTTYPE).so
CFLAGS= -g -Wall -Werror -Wextra
CPATH= src/
OPATH= obj/
HPATH= inc/
INC= $(addprefix -I, $(HPATH))
CFILES= malloc.c \
		allocation.c \
		free.c \
		show_alloc_mem.c \
		realloc.c

OFILES= $(CFILES:.c=.o)

HFILES= inc/malloc.h

OBJ= $(addprefix $(OPATH), $(OFILES))

.PHONY: all clean fclean name re

all: $(NAME)

$(NAME): $(OBJ)
	$(CC) -shared $(CFLAGS) $(OBJ) -o $(NAME)
	@ln -s $(NAME) libft_malloc.so

$(OPATH)%.o: $(CPATH)%.c $(HFILES)
	@mkdir -p $(OPATH)
	$(CC) -g -Wall -Werror -Wextra  $(INC) $< -c -o $@

clean:
	rm -rf $(OBJ)

fclean: clean
	rm -rf $(NAME)
	rm -rf libft_malloc.so

re: fclean all
	