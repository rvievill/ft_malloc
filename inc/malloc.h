/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <rvievill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 10:38:52 by rvievill          #+#    #+#             */
/*   Updated: 2017/12/12 18:02:15 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef MALLOC_H
# define MALLOC_H

# include <unistd.h>
# include <sys/mman.h>
# include <limits.h>
# include <pthread.h>

# define TINY (size_t)(4096 * 2)
# define SMALL (size_t)(4096 * 16)
# define Z_TINY (size_t)(100 * (TINY + BLOCK_SIZE))
# define Z_SMALL (size_t)(100 * (SMALL + BLOCK_SIZE))
# define BLOCK_SIZE sizeof(t_block)

typedef unsigned long int	t_uli;

typedef struct		s_block
{
	size_t			size;
	size_t			size_max;
	int				free;
	struct s_block	*start;
	struct s_block	*next;
	char			data[0];
}					t_block;

typedef struct		s_head
{
	t_block			*tiny;
	t_block			*small;
	t_block			*large;
	t_uli			total;
}					t_head;

t_head				g_base;

void				show_alloc_mem(void);
void				free(void *ptr);
t_block				*new_alloc(t_block **base, size_t size, size_t zone);
void				*malloc(size_t size);
void				*realloc(void *ptr, size_t size);
t_block				*valid_addr(void *ptr, t_block *type);
#endif
