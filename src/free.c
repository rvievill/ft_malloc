/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   free.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <rvievill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/04 17:12:52 by rvievill          #+#    #+#             */
/*   Updated: 2017/12/12 14:38:10 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_block				*valid_addr(void *ptr, t_block *type)
{
	t_block			*tmp;

	tmp = type;
	while (tmp)
	{
		if (tmp == ptr - BLOCK_SIZE)
			return (type);
		tmp = tmp->next;
	}
	return (NULL);
}

void				free(void *ptr)
{
	t_block			*block;

	block = NULL;
	if (!(block = valid_addr(ptr, g_base.tiny))
	&& !(block = valid_addr(ptr, g_base.small))
	&& !(block = valid_addr(ptr, g_base.large)))
		return ;
	while (block)
	{
		if (block->data == ptr)
		{
			if (block->size <= SMALL)
				block->free = 1;
			else
				munmap(ptr, block->size);
			return ;
		}
		block = block->next;
	}
}
