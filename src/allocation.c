/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   allocation.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <rvievill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/22 19:11:16 by rvievill          #+#    #+#             */
/*   Updated: 2017/12/12 13:11:38 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

t_block				*new_alloc(t_block **base, size_t size, size_t zone)
{
	t_block			*new;

	new = mmap(0, zone, PROT_READ | PROT_WRITE, MAP_ANON | MAP_PRIVATE, -1, 0);
	new->size = size;
	new->size_max = zone;
	new->start = new;
	new->next = NULL;
	new->free = 0;
	if (base && *base)
		(*base)->next = new;
	return (new);
}
