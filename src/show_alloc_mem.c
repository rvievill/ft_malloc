/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   show_alloc_mem.c                                   :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <rvievill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 15:11:43 by rvievill          #+#    #+#             */
/*   Updated: 2017/12/12 18:10:55 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

void			print_memory(t_uli addr)
{
	if (addr > 9)
		print_memory(addr / 16);
	addr = addr % 16 < 10 ? addr % 16 + 48 : addr % 16 + 55;
	write(1, &addr, 1);
}

void			ft_putchar(char c)
{
	write(1, &c, 1);
}

void			ft_putnbr(t_uli nb)
{
	if (nb > 9)
		ft_putnbr(nb / 10);
	ft_putchar('0' + (nb % 10));
}

void			print_all_list(t_block *lst)
{
	t_block		*tmp;
	t_uli		addr;

	tmp = lst;
	while (tmp)
	{
		addr = (t_uli)tmp->data;
		write(1, "0x", 2);
		print_memory(addr);
		write(1, " - ", 3);
		write(1, "0x", 2);
		print_memory(addr + tmp->size);
		write(1, " : ", 3);
		ft_putnbr(tmp->size);
		write(1, " octets\n", 8);
		tmp = tmp->next;
	}
}

void			show_alloc_mem(void)
{
	write(1, "TINY : 0x", 9);
	print_memory((t_uli)g_base.tiny);
	write(1, "\n", 1);
	print_all_list(g_base.tiny);
	write(1, "SMALL : 0x", 10);
	print_memory((t_uli)g_base.small);
	write(1, "\n", 1);
	print_all_list(g_base.small);
	write(1, "LARGE : 0x", 10);
	print_memory((t_uli)g_base.large);
	write(1, "\n", 1);
	print_all_list(g_base.large);
	write(1, "Total: ", 7);
	ft_putnbr(g_base.total);
	write(1, " octets\n", 8);
}
