/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   malloc.c                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <rvievill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 10:47:31 by rvievill          #+#    #+#             */
/*   Updated: 2018/01/05 13:24:52 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"
#include <sys/resource.h>

static void				find_block(t_block **last, size_t size)
{
	t_block				*cur;

	cur = *last;
	while (cur)
	{
		*last = cur;
		if (cur->size >= size && cur->free)
			break ;
		cur = cur->next;
	}
}

static t_block			*split_block(t_block *block, size_t size)
{
	t_block				*new;

	new = (t_block *)(block->data + block->size + 4);
	new->size_max = block->size_max;
	new->size = size;
	new->next = block->next;
	new->start = block->start;
	new->free = 0;
	block->next = new;
	return (new);
}

static void				*alloc_mem(t_block **base, size_t zone, size_t size)
{
	t_block				*last;

	if (!(*base))
	{
		*base = new_alloc(base, size, zone);
		last = *base;
	}
	else
	{
		last = *base;
		find_block(&last, size);
		if (last->size >= size && last->free)
			last->free = 0;
		else if ((t_uli)last->start + last->size_max - (t_uli)last >=
		BLOCK_SIZE + size)
			last = split_block(last, size);
		else
		{
			last->next = new_alloc(&last, size, zone);
			last = last->next;
		}
	}
	g_base.total += size;
	return (last->data);
}

void					*malloc(size_t size)
{
	if (size > (SIZE_T_MAX - (getpagesize() * 2)))
		return (NULL);
	if (size <= TINY)
		return (alloc_mem(&(g_base.tiny), Z_TINY, size));
	else if (size > TINY && size <= SMALL)
		return (alloc_mem(&(g_base.small), Z_SMALL, size));
	else
		return (alloc_mem(&(g_base.large), size, size));
}
