/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   realloc.c                                          :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: rvievill <rvievill@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/07 17:57:54 by rvievill          #+#    #+#             */
/*   Updated: 2018/01/05 13:25:00 by rvievill         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "malloc.h"

static void				ft_memcpy(void *dst, const void *src)
{
	size_t				i;
	unsigned char		*dest;
	const unsigned char	*s;

	i = 0;
	dest = (unsigned char*)dst;
	s = (const unsigned char*)src;
	while (s[i])
	{
		dest[i] = s[i];
		i++;
	}
}

void					*realloc(void *ptr, size_t size)
{
	t_block				*block;
	t_block				*new;

	if (!ptr)
		return (malloc(size));
	if ((block = valid_addr(ptr, g_base.tiny))
	|| (block = valid_addr(ptr, g_base.small))
	|| (block = valid_addr(ptr, g_base.large)))
	{
		while (block->next && block->data != ptr)
			block = block->next;
		if (block->size >= size)
			return (block->data);
		new = malloc(size);
		if (!new)
			return (NULL);
		ft_memcpy(new, block->data);
		free(block);
		return (new);
	}
	return (NULL);
}
